package matejbot

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch" //použito kvuli Alojz - https://cloud.google.com/appengine/docs/go/issue-requests
	"strings"
	"log"
	"io/ioutil"
	"encoding/xml"
)

var inMyHand string = "nic"
var inMyHandCounter int = 0
var inMyHandCounterStr string = "0"
var kamenNuzkyPapir int = 0
var chatCoins, chatXP int = 0, 0

type Response struct {
	Messages []interface{} `json:"messages"`
}

type TextMessage struct {
	Text string `json:"text"`
}

func init() {
	http.HandleFunc("/today", today)
	http.HandleFunc("/vocative", vocative)
	http.HandleFunc("/showimage", showImage)
	http.HandleFunc("/showvideo", showVideo)

	http.HandleFunc("/inhand", inHand)
	//http.HandleFunc("/kamen", gameKamen)
	http.HandleFunc("/kamenDb", gameKamenDb)
	http.HandleFunc("/kamenDbEn", gameKamenDbEn)

	http.HandleFunc("/changeNickname", changeNickname)

	//http.HandleFunc("/addChatCoins", addChatCoins)
	//http.HandleFunc("/showChatCoins", showChatCoins)

	http.HandleFunc("/getUserInfo", getUserInfo) //načte informace od uživatele
	//https://matej-dot-capsabot.appspot.com/getUserInfo?userId={{fb_id}}&Name={{fb_first_name}}&Surname={{fb_last_name}}&TimeZone{{fb_timezone}}&Gender={{fb_gender}}&Locale={{fb_locale}}
	http.HandleFunc("/add-coins", addCoins)   //přidá do DB uživatele xx Coins ( ?userId=martina&amount=11)
	http.HandleFunc("/show-coins", showCoins) //zobrazí stav DB uživatele


	http.HandleFunc("/botPrintText", botPrintText) //zobrazí text v BOTu z aargumentem "text"



	http.HandleFunc("/getWeather", getWeather) //zobrazí počasí ve městech
	http.HandleFunc("/getWeatherXML", getWeatherXML) //zobrazí počasí v Brně z XML




}

//vytvori App Engine Context v namespace "matejbot"

func AppEngineContext(r *http.Request) (context.Context, error) {
	ctx := appengine.NewContext(r)
	//do Datastore budeme ukladat pod namespace "matejbot",
	//momentalne v capsabot aplikaci bezi vice modulu,
	//takto se nam nebudou michat data
	return appengine.Namespace(ctx, "matejbot")
}


func getUserInfo(w http.ResponseWriter, r *http.Request) {
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
  	userName := r.URL.Query().Get("Name") // z Chatfuelu jméno uživatele
	userSurname := r.URL.Query().Get("Surname") // z Chatfuelu příjmení uživatele
	userTimeZone := r.URL.Query().Get("TimeZone") // z Chatfuelu časovou zónu
	userGender := r.URL.Query().Get("Gender") // z Chatfuelu Gender
	userLocale := r.URL.Query().Get("Locale") // z Chatfuelu jazyk uživatele
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// tady je možné měnit parametry uživatele...
	user.Name = userName
	user.Surname = userSurname
	user.TimeZone = userTimeZone
	user.Gender = userGender
	user.Locale = userLocale
	user.LastSeen = time.Now()


	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

//prazdny JSON pro chatFuel
w.Write([]byte("{}"))

}


//přidá coins podle userId
func addCoins(w http.ResponseWriter, r *http.Request) {
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	userIdAmout := r.URL.Query().Get("amount") // kolik připsat coinu
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// tady je potreba neco uselat s promennou user, treba zvysit pocet coinu ;-)

	amoutOfCoins, err := strconv.Atoi(userIdAmout)
	user.Coins = user.Coins + amoutOfCoins


	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

//prazdny JSON pro chatFuel
w.Write([]byte("{}"))
}



func showCoins(w http.ResponseWriter, r *http.Request) {
	var vystupniText string = ""
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}


	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}


switch user.Locale {
case "cs_CZ":
	vystupniText = "Stav účtu je "
default:
	vystupniText = "The balance is "
}

vystupniText = vystupniText + strconv.Itoa(user.Coins)

res := Response{
	Messages: []interface{}{
		TextMessage{Text: vystupniText},
	},
}

payload, err := json.Marshal(res)
if err != nil {
	w.WriteHeader(http.StatusInternalServerError)
	return
}
w.Write(payload)
}



//změní Nickname usera - pokud má dost Coins
func changeNickname(w http.ResponseWriter, r *http.Request) {
	var vystupniText string = ""
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	userIdNickname := r.URL.Query().Get("changeNickname") // nový nickname
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// tady je potreba neco uselat s promennou user, treba zvysit pocet coinu ;-)

	switch user.Locale {
	case "cs_CZ":
		vystupniText = "Nemáš dostatek ChatCoinů."
	default:
		vystupniText = "Your don't have enought ChatCoins."
	}

	if user.Coins > 100 {
		user.Nickname = userIdNickname
		user.Coins = user.Coins - 100

		switch user.Locale {
		case "cs_CZ":
			vystupniText = "Tvé oslovení bylo změněno na: " + userIdNickname
		default:
			vystupniText = "Your new nickname is " + userIdNickname
		}

	}


	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: vystupniText},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)

}


//zobrazí text z argumentu
func botPrintText(w http.ResponseWriter, r *http.Request) {

	botText:= r.URL.Query().Get("text")  //text k vytištění


	finalCode := `{"messages":[ {"text": "` + botText + `"},]}`


	res := finalCode



	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)


}







//zobrazí počasí z Alojz v zadaném městě, nebo v Brně
	func getWeather(w http.ResponseWriter, r *http.Request) {


	//nové dva typy pro uložení počasí
	type dayWeather struct {
		DateOfForecast  string `json:"date"`
		DateAndPlace    string `json:"Place_date_string"`
		DateCreated     string `json:"created"`
		TodayOrTomorrow string `json:"today_tomorrow"`
		ForecastText    string `json:"string"`
	}
	type weatherAlojz struct {
		Code           int        `json:"code"`
		PreferedDay    string     `json:"prefer"`
		ForcastForDay2 dayWeather `json:"day2"`
		ForcastForDay1 dayWeather `json:"day1"`
		AlojzVersion   string     `json:"version"`
		AlojzCommand   string     `json:"command"`
		WeatherCity    string     `json:"url_id"`
	}


	var vystupniText string = ""
	cityWeather := r.URL.Query().Get("city")  //město pro předpověď počasí
	cityWeather = strings.ToLower(cityWeather) //mesto převede na malá písmena - POZOR NA DIAKRITIKU: NEUMÍ TO




//	func handler(w http.ResponseWriter, r *http.Request) {
//toto jsem musel udělat, ale nevím pořádně proč  ani jak to funguje: https://cloud.google.com/appengine/docs/go/issue-requests

		ctx := appengine.NewContext(r)
		client := urlfetch.Client(ctx)
		resp, err := client.Get("https://alojz.cz/api/v1/solution?url_id=/" + cityWeather)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		bs, err := ioutil.ReadAll(resp.Body)
		defer resp.Body.Close()
		if err != nil {
			log.Fatal(err)
		}


/*	*** na místo tohoto:

	resWeather, errWeather := http.Get("https://alojz.cz/api/v1/solution?url_id=/" + cityWeather)
	if errWeather != nil {
		log.Fatal(errWeather)
	}
	bs, errWeather := ioutil.ReadAll(resWeather.Body)
	defer resWeather.Body.Close()

	if errWeather != nil {
		log.Fatal(errWeather)
	}

*/



	//analyzuje počasi





	var weatherForecast weatherAlojz
	var todayForecast dayWeather

	dataFromAlojz := []byte(bs)

	json.Unmarshal(dataFromAlojz, &weatherForecast)

	//do struktury weatherForecast načte předpověd počasí na dva dny
	//ve strukture todayForecast je na dnešek - dle přeferovaného dne

	todayForecast = todayForecast

	if weatherForecast.PreferedDay == "day1" {
		todayForecast = weatherForecast.ForcastForDay1
	} else {
		todayForecast = weatherForecast.ForcastForDay2
	}

	//analyzuje počasi - konec



	vystupniText = todayForecast.DateAndPlace + ": "+ todayForecast.ForecastText


	res := Response{
		Messages: []interface{}{
			TextMessage{Text: vystupniText},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}




func getWeatherXML(w http.ResponseWriter, r *http.Request) {


	/* načtení počasí z XML
	IMG:https://symbol.yr.no/grafikk/sym/b38/01d.png
	https://www.yr.no/place/Czech_Republic/South_Moravia/Brno/
	http://www.yr.no/place/Czech_Republic/South_Moravia/Brno/forecast_hour_by_hour.xml

	*/

//nové typy pro XML předpoved

	type PlaceLocation struct {
		PlaceAltitude string `xml:"altitude,attr"`
		PlaceLatitude string `xml:"latitude,attr"`
		PlaceLongitude string `xml:"longitude,attr"`
		PlaceGeobase string `xml:"geobase,attr"`
		PlaceGeobaseid string `xml:"geobaseid,attr"`
	}

	type PlaceTimezone struct {
		PlaceID string `xml:"id,attr"`
		PlaceUTC string `xml:"utcoffsetMinutes,attr"`
	}

	type LocationWeather struct {
		XMLName xml.Name 	`xml:"location"`
		PlaceName 		string 	`xml:"name"`
		PlaceType		string `xml:"type"`
		PlaceCountry           	string `xml:"country"`
		PlaceTimezone           PlaceTimezone`xml:"timezone"`
		PlaceLocation           PlaceLocation `xml:"location"`
	}

	type MetaWeather struct {
		XMLName xml.Name 	`xml:"meta"`
		Lastupdate 		string 	`xml:"lastupdate"`
		Nextupdate		string `xml:"nextupdate"`
	}

	type DetailSymbol struct {
		DetailNumber string `xml:"number,attr"`
		DetailNumberEx string `xml:"numberEx,attr"`
		DetailName string `xml:"name,attr"`	//Clear sky
		DetailIcon string `xml:"var,attr"`	//logo ikony
	}

	type DetailWindDirection struct {
		DetailDirection string `xml:"deg,attr"` //22.9
		DetailCode string `xml:"code,attr"` //NNE
		DetailName string `xml:"name,attr"` //North-northeast
	}

	type DetailWindSpeed struct {
		DetailSpeed string `xml:"mps,attr"` //0.7
		DetailName string `xml:"name,attr"` //Light air
	}

	type DetailTemperature struct {
		DetailUnits string `xml:"unit,attr"` //celsius
		DetailValue string `xml:"value,attr"` //3
	}

	//detail předpovědi počasí pro následující hodinu
	type DetailSituation struct {
		DetailSymbol 		DetailSymbol 	`xml:"symbol"`
		DetailWindDirection 	DetailWindDirection 	`xml:"windDirection"`
		DetailWindSpeed 	DetailWindSpeed 	`xml:"windSpeed"`
		DetailTemperature 	DetailTemperature 	`xml:"temperature"`
	}

	type TabularWeather struct {
		//	XMLName xml.Name 	`xml:"forecast"`
		DetailSituation 	[]DetailSituation	`xml:"time"`
	}

	type ForecastWeather struct {
		//	XMLName xml.Name 	`xml:"forecast"`
		TabularWeather 		TabularWeather 	`xml:"tabular"`
	}

	type WeatherForecast struct {
		XMLName xml.Name 	`xml:"weatherdata"`
		LocationWeather 	LocationWeather `xml:"location"`
		MetaWeather 		MetaWeather `xml:"meta"`
		ForecastWeather		ForecastWeather `xml:"forecast"`
	}



	var vystupniText string = ""

	const URLWeatherXML  = "http://www.yr.no/place/Czech_Republic/South_Moravia/Brno/forecast_hour_by_hour.xml"

	ctx := appengine.NewContext(r)
	client := urlfetch.Client(ctx)
	resp, err := client.Get(URLWeatherXML)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	bs, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		log.Fatal(err)
	}


	var actualWeather WeatherForecast

	err2 := xml.Unmarshal([]byte(bs	), &actualWeather)
	if err2 != nil {
		fmt.Printf("error: %v", err)
		return
	}

	//analyzuje počasi


	aktualniTeplota:= actualWeather.ForecastWeather.TabularWeather.DetailSituation[0].DetailTemperature.DetailValue
	aktualniRychlostVetru:=actualWeather.ForecastWeather.TabularWeather.DetailSituation[0].DetailWindSpeed.DetailSpeed
	vystupniText = "Aktuální teplota v Brně je "+ aktualniTeplota + "\u00b0C, vítr fouká rychlostí " + aktualniRychlostVetru + "km/h"

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: vystupniText},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}



// stare od martina ******************

/*
func addChatCoins(w http.ResponseWriter, r *http.Request) {
//funkce, která ti přičte a zobrazí počet chatCoins....

	names := r.URL.Query()["amount"]
	if len(names) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	amoutOfCoins, err := strconv.Atoi(names[0])

	chatCoins = chatCoins + amoutOfCoins
	//chatCoins ++

	chatCoinsStr := strconv.Itoa(chatCoins)


	res := Response{
		Messages: []interface{}{
			TextMessage{Text: "Byl ti přidán " + names[0] + " ChatCoin a celkem máš " + chatCoinsStr},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

func showChatCoins(w http.ResponseWriter, r *http.Request) {
//funkce, která ti zobrazí  počet chatCoins....


	chatCoinsStr := strconv.Itoa(chatCoins)

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: chatCoinsStr},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

*/

func inHand(w http.ResponseWriter, r *http.Request) {
	names := r.URL.Query()["item"]
	if len(names) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	name := names[0] //do name se uloží nový předmět
	inMyHandCounterStr := strconv.Itoa(inMyHandCounter)

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: "v ruce byl " + inMyHand + ", ale už je tam " + name + ". Uchopil jsem už " + inMyHandCounterStr + "krát"},
		},
	}

	inMyHand = name //uloží poslední předmět do proměné
	inMyHandCounter++

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}


//zahraje hru kámen-nůžky papír a připočte odměnu
func gameKamenDb(w http.ResponseWriter, r *http.Request) {
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	item := r.URL.Query().Get("item") // z chatFuel natáhne položku výběru člověka
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// tady je začátek hry
	cl := 0            //zadání člověk
	pc := rand.Intn(3) //zadání pc - náhoda
	pcVypsani := strconv.Itoa(pc)
	stavHry := ""

	switch pc {
	case 0:
		pcVypsani = "kámen"
	case 1:
		pcVypsani = "nůžky"
	default:
		pcVypsani = "papír"
	}

	switch item {
	case "kamen":
		cl = 0
	case "nuzky":
		cl = 1
	default:
		cl = 2
	}

	switch {
	case (pc == 0) && (cl == 1):
		stavHry = "prohrál"
	case (pc == 0) && (cl == 2):
		stavHry = "vyhrál"
	case (pc == 1) && (cl == 0):
		stavHry = "vyhrál"
	case (pc == 1) && (cl == 2):
		stavHry = "prohrál"
	case (pc == 2) && (cl == 0):
		stavHry = "prohrál"
	case (pc == 2) && (cl == 1):
		stavHry = "vyhrál"
	default:
		stavHry = "remízoval"
	}

	//připsání kreditu chatCoins
		switch stavHry {
		case "prohrál":
			if user.Coins > 10 {
				user.Coins = user.Coins - 10
			} else {
				user.Coins = 0
			}
		case "vyhrál":
			user.Coins = user.Coins + 10
		default:
			user.Coins ++
		}

		stavUctu := strconv.Itoa(user.Coins)

		res := Response{
			Messages: []interface{}{
				TextMessage{Text:" Já jsem dal " + pcVypsani + " a ty jsi " + stavHry + ". Nový stav účtu je " + stavUctu},
			},
		}


		payload, err := json.Marshal(res)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(payload)


	// tady je konec hry


	// tady uložení do DB
	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
//prazdny JSON pro chatFuel
//w.Write([]byte("{}"))
}

//english version - anglická verze Kámen....
func gameKamenDbEn(w http.ResponseWriter, r *http.Request) {
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	item := r.URL.Query().Get("item") // z chatFuel natáhne položku výběru člověka
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// tady je začátek hry
	cl := 0            //zadání člověk
	pc := rand.Intn(3) //zadání pc - náhoda
	pcVypsani := strconv.Itoa(pc)
	stavHry := ""

	switch pc {
	case 0:
		pcVypsani = "rock"
	case 1:
		pcVypsani = "scissors"
	default:
		pcVypsani = "paper"
	}

	switch item {
	case "rock":
		cl = 0
	case "scissors":
		cl = 1
	default:
		cl = 2
	}

	switch {
	case (pc == 0) && (cl == 1):
		stavHry = "lose"
	case (pc == 0) && (cl == 2):
		stavHry = "win"
	case (pc == 1) && (cl == 0):
		stavHry = "win"
	case (pc == 1) && (cl == 2):
		stavHry = "lose"
	case (pc == 2) && (cl == 0):
		stavHry = "lose"
	case (pc == 2) && (cl == 1):
		stavHry = "win"
	default:
		stavHry = "draw"
	}

	//připsání kreditu chatCoins
		switch stavHry {
		case "lose":
			if user.Coins > 10 {
				user.Coins = user.Coins - 10
			} else {
				user.Coins = 0
			}
		case "win":
			user.Coins = user.Coins + 10
		default:
			user.Coins ++
		}

		stavUctu := strconv.Itoa(user.Coins)

		res := Response{
			Messages: []interface{}{
				TextMessage{Text:" My throw: " + pcVypsani + " ...and you " + stavHry + ". Your new balance is " + stavUctu},
			},
		}


		payload, err := json.Marshal(res)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(payload)


	// tady je konec hry


	// tady uložení do DB
	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
//prazdny JSON pro chatFuel
//w.Write([]byte("{}"))
}
// konec anglické verze

/*
func gameKamen(w http.ResponseWriter, r *http.Request) {
	names := r.URL.Query()["item"]
	cl := 0            //zadání člověk
	pc := rand.Intn(3) //zadání pc - náhoda
	pcVypsani := strconv.Itoa(pc)
	stavHry := ""

	if len(names) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	item := names[0] //do itemse uloží předmět člověk

	switch pc {
	case 0:
		pcVypsani = "kámen"
	case 1:
		pcVypsani = "nůžky"
	default:
		pcVypsani = "papír"
	}

	switch item {
	case "kamen":
		cl = 0
	case "nuzky":
		cl = 1
	default:
		cl = 2
	}

	switch {
	case (pc == 0) && (cl == 1):
		stavHry = "prohrál"
	case (pc == 0) && (cl == 2):
		stavHry = "vyhrál"
	case (pc == 1) && (cl == 0):
		stavHry = "vyhrál"
	case (pc == 1) && (cl == 2):
		stavHry = "prohrál"
	case (pc == 2) && (cl == 0):
		stavHry = "prohrál"
	case (pc == 2) && (cl == 1):
		stavHry = "vyhrál"
	default:
		stavHry = "remízoval"
	}

//připsání kreditu chatCoins
	switch stavHry {
	case "prohrál":
		if chatCoins > 10 {
			chatCoins = chatCoins - 10
		} else {
			chatCoins = 0
		}
	case "vyhrál":
		chatCoins = chatCoins + 10
	default:
		chatCoins ++
	}

	stavUctu := strconv.Itoa(chatCoins)

	res := Response{
		Messages: []interface{}{
			TextMessage{Text:" Já jsem dal " + pcVypsani + " a ty jsi " + stavHry + ". Nový stav účtu je " + stavUctu},
		},
	}


	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

*/


func today(w http.ResponseWriter, r *http.Request) {
	actualTime := time.Now().Add(60 * time.Minute) //....zjistím čas a připočítám 60 minut
	//day := time.Now().Weekday()  ..... puvodni
	day := actualTime.Weekday() // ..... vrátí den v týdnu
	text := fmt.Sprintf("Dnes je"+" %v. Čas je "+actualTime.Format("15:04"), translateDayToCzech(day))
	res := Response{
		Messages: []interface{}{
			TextMessage{Text: text},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

func showImage(w http.ResponseWriter, r *http.Request) {

	res := Response{
		Messages: []interface{}{
			map[string]interface{}{
				"attachment": map[string]interface{}{
					"type": "image",
					"payload": map[string]interface{}{
						"url": "https://images.webcams.travel/webcam/1206291667-Weather-Brno-%E2%80%BA-West%3A-Malinovsk%C3%A9ho-n%C3%A1m%C4%9Bst%C3%AD-%28Mahenovo-divadlo%29-Brno.jpg",
						//						"url": "https://images.webcams.travel/thumbnail/0e143137e59a902e96b3a93b733f78db/1206291667.jpg",

					},
				},
			},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

func showVideo(w http.ResponseWriter, r *http.Request) {

	res := Response{
		Messages: []interface{}{
			map[string]interface{}{
				"attachment": map[string]interface{}{
					"type": "video",
					"payload": map[string]interface{}{
						"url": "http://krcek.cz/wp-content/uploads/2016/12/Alan-Walker-Faded_2.mp4",
					},
				},
			},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
}

func vocative(w http.ResponseWriter, r *http.Request) {
	var name = ""
	userId := r.URL.Query().Get("userId") // z Chatfuelu je potreba poslat napr. uzivatelovo facebookId v parametru userId
	ctx, err := AppEngineContext(r) //context (a.k.a. ctx) se pouziva pro veskerou praci s App Engine sluzbama
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	user, err := LoadUser(ctx, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(user.Name) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}


	switch user.Nickname {
	case "":
		name = translateNameToCzech(user.Name)
	default:
		name = user.Nickname
	}

	//name := translateNameToCzech(user.Name)

	if err = SaveUser(ctx, user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: name},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)


	/* původní
	names := r.URL.Query()["name"]
	if len(names) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	name := translateNameToCzech(names[0])

	res := Response{
		Messages: []interface{}{
			TextMessage{Text: name},
		},
	}

	payload, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(payload)
	*/
}

func translateNameToCzech(name string) string {
	switch name {
	case "Matěj":
	  return "Matěji"
	case "Matej":
		return "Matěji"
	case "Mia":
		return "Mio"
	case "Adam":
		return "Adame"
	case "Filip":
		return "Filipe"
	case "Filda":
		return "Fildo"
	case "David":
		return "Davide"
	case "Jarek":
		return "Jarku"
	case "Šimon":
		return "Šimone"
	case "Mirek":
		return "Mirku"
	case "Massimo":
		return "Massimo"
	case "Gabča":
		return "Gabčo"
	case "Patrik":
		return "Patriku"
	case "Nela":
		return "Nelo"
	case "Míša":
		return "Míšo"
	case "Michaela":
		return "Míšo"
	case "Michala":
		return "Míšo"
	case "Petr":
		return "Petře"
	case "Martin":
		return "Martine"
	case "Pavlína":
		return "Pavlíno"
	case "Dominik":
		return "Dominiku"
	case "Eliška":
		return "Eliško"
	case "Massi":
		return "Massimo"
	case "Sisi":
		return "Sisko"
	default:
		return name
	}
}

func translateDayToCzech(weekday time.Weekday) string {
	switch weekday {
	case time.Monday:
		return "pondělí"
	case time.Tuesday:
		return "úterý"
	case time.Wednesday:
		return "středa"
	case time.Thursday:
		return "čtvrtek"
	case time.Friday:
		return "pátek"
	case time.Saturday:
		return "sobota"
	case time.Sunday:
		return "neděle"
	default:
		return "?"
	}
}
