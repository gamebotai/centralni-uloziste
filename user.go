package matejbot

import (
	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"time"

)

type User struct {
	Id    string `datastore:"-"`        //neuklada se do Datastore, pouze informacni
	Coins int    `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	Name string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	Surname string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	TimeZone string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	Locale string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	Gender string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	LastSeen time.Time `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba
	Nickname string `datastore:",noindex"` //vypnuto vyhledavani v Datastore podle Coins -> zatim neni potreba

}

//Nacte data uzivatele podle userId z Datastore, pokud neexistuje, vytvori se novy
func LoadUser(ctx context.Context, userId string) (*User, error) {
	user := User{Id: userId}
	if err := datastore.Get(ctx, userKey(ctx, userId), &user); err != nil && err != datastore.ErrNoSuchEntity {
		return nil, err
	}
	return &user, nil
}

func SaveUser(ctx context.Context, user *User) error {
	if _, err := datastore.Put(ctx, userKey(ctx, user.Id), user); err != nil {
		return err
	}
	return nil
}

func DeleteUser(ctx context.Context, userId string) error {
	return datastore.Delete(ctx, userKey(ctx, userId))
}

//Interni metoda, vytvori klic k entite typu "User"
func userKey(ctx context.Context, userId string) *datastore.Key {
	return datastore.NewKey(ctx, "User", userId, 0, nil)
}
